module.exports = function(config) {
  config.set({

    basePath: '',
    frameworks: ['mocha', 'chai', 'sinon'],

    files: [
      'node_modules/angular/angular.min.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'node_modules/angularfire/dist/angularfire.min.js',
      'js/vendor/lvl-uuid.js',
      'js/vendor/lvl-drag-drop.js',
      'node_modules/ng-dialog/js/ngDialog.min.js',
      'node_modules/angular-aria/angular-aria.min.js',
      'node_modules/angular-sanitize/angular-sanitize.min.js',
      'node_modules/ng-file-upload/dist/ng-file-upload-all.min.js',
      'node_modules/papaparse/papaparse.min.js',
      'js/**/*.js',
      'test/**/*Test.js',
    ],

    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'js/*.js': ['coverage']
    },
    coverageReporter: {
      repoToken: 'QVdqIxSZvbUFLmSiYZ3uINtguZxhuBgy7',
      type: 'lcov',
      dir: 'coverage/'
    },
    junitReporter: {
      outputDir: 'test-reports/', // results will be saved as $outputDir/$browserName.xml
      outputFile: undefined, // if included, results will be saved as $outputDir/$browserName/$outputFile
      suite: '', // suite will become the package name attribute in xml testsuite element
      useBrowserName: true, // add browser name to report and classes names
      nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
      classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
      properties: {}, // key value pair of properties to add to the <properties> section of the report
      xmlVersion: null // use '1' if reporting to be per SonarQube 6.2 XML format
    },

    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['junit', 'coverage'],

    // web server port
    port: 9876,
    colors: true,

    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_DEBUG,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity,
    plugins: [
      'karma-chai',
      'karma-mocha',
      'karma-phantomjs-launcher',
      'karma-coverage',
      'karma-nyan-reporter',
      'karma-mocha-reporter',
      'karma-junit-reporter',
      'karma-sinon',
    ]
  })
}
